//wrapper function
module.exports = function(grunt) {
    //project definition and tasks writing section
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            options: {
                style: 'expanded',
                sourcemap: 'none',
                noCache: true

            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'sass/',
                    dest: 'css/',
                    src: '*.scss',
                    ext: '.css'
                }]

            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass'],
                options: {
                    livereload: true
                }
            }
        },
        express: {
            all: {
                options: {
                    port: 6000,
                    hostname: 'localhost',
                    bases: ['.'],
                    livereload: true
                }
            }
        },
        copy: {
            main: {
                files: [{
                    src: ['bower_components/jquery/dist/jquery.min.js'],
                    dest: 'js/jquery.min.js'
                }, {
                    src: ['bower_components/bootstrap/dist/css/bootstrap.min.css'],
                    dest: 'css/bootstrap.min.css'
                }, {
                    src: ['bower_components/angular/angular.min.js'],
                    dest: 'js/angular.min.js'
                }],
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-express');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['express', 'sass', 'watch']);
    grunt.registerTask('build', ['copy']);

};
//wrapper function ends
